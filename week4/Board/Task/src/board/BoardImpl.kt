package board

import board.Direction.*

class MySquareBoard(override val width: Int) : SquareBoard {

    private val elements: Array<Array<Cell>> = Array(width, init = initFirstDimension())

    private fun initFirstDimension() = { i: Int -> Array(width, init = initSecondDimension(i)) }
    private fun initSecondDimension(i: Int) = { j: Int -> Cell(i + 1, j + 1) }

    private fun notInRange(value: Int): Boolean {
        return value < 1 || value > width
    }

    private fun notInRange(i: Int, j: Int): Boolean {
        return notInRange(i) || notInRange(j)
    }

    override fun getCellOrNull(i: Int, j: Int): Cell? {
        return if(notInRange(i, j)) {
            null
        } else {
            elements[i-1][j-1]
        }
    }

    override fun getCell(i: Int, j: Int): Cell {
        if(notInRange(i, j)) {
            throw IllegalArgumentException("cell out of bound. got i: $i, j: $j expecting $width")
        }

        return elements[i-1][j-1]
    }

    override fun getAllCells(): Collection<Cell> {
        return elements
                .flatMap { secondDimensionCells -> secondDimensionCells.asList() }
                .toList()
    }

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> {
        if(notInRange(i)) {
            return emptyList()
        }

        return jRange.mapNotNull { j ->
            if (notInRange(j)) {
                null
            } else {
                elements[i-1][j-1]
            }
        }
    }

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> {
        if(notInRange(j)) {
            return emptyList()
        }

        return iRange.mapNotNull { i ->
            if (notInRange(i)) {
                null
            } else {
                elements[i-1][j-1]
            }
        }
    }

    override fun Cell.getNeighbour(direction: Direction): Cell? {
        var i = this.i
        var j = this.j

        when(direction) {
            DOWN -> i++
            UP -> i--
            LEFT -> j--
            RIGHT -> j++
        }

        return if(notInRange(i, j)) {
            null
        } else {
            elements[i-1][j-1]
        }
    }
}

class MyGameBoard<T>(private val squareBoard: SquareBoard, override val width: Int) :
        SquareBoard by squareBoard, GameBoard<T> {

    private val values = mutableMapOf<Cell, T?>()

    override fun get(cell: Cell): T? {
        return values[cell]
    }

    override fun set(cell: Cell, value: T?) {
        values[cell] = value
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> {
        return values
                .filterValues(predicate)
                .keys
    }

    override fun find(predicate: (T?) -> Boolean): Cell? {
        return values
                .filterValues(predicate)
                .keys.first()
    }

    override fun any(predicate: (T?) -> Boolean): Boolean {
        return squareBoard
                .getAllCells()
                .map { cell -> values.getOrDefault(cell, null) }
                .any(predicate)
    }

    override fun all(predicate: (T?) -> Boolean): Boolean {
        return squareBoard
                .getAllCells()
                .map { cell -> values.getOrDefault(cell, null) }
                .filterNot(predicate)
                .count() == 0
    }

}

fun createSquareBoard(width: Int): SquareBoard = MySquareBoard(width)
fun <T> createGameBoard(width: Int): GameBoard<T> = MyGameBoard<T>(MySquareBoard(width), width)

