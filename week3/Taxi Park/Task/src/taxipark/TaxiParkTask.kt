package taxipark

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> {
    val activeDrivers = this.trips.map { it.driver }.distinct()

    return this.allDrivers.minus(activeDrivers).toSet()
}

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> {
    if(minTrips == 0) {
        return this.allPassengers
    }

    return this
            .trips
            .flatMap(Trip::passengers)
            .groupBy { passenger -> passenger }
            .filterValues { passengerTrips -> passengerTrips.size >= minTrips }
            .keys
}


/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> {
    return this
            .trips
            .filter { trip -> trip.driver == driver }
            .flatMap { trip -> trip.passengers }
            .groupBy { passenger -> passenger }
            .filterValues { passengerTripsWithDriver -> passengerTripsWithDriver.size > 1 }
            .keys
}


/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> {
    return this
            .trips
            .flatMap(passengersHaveDiscountInTrip())
            .groupBy(keySelector = { it.first}, valueTransform = { it.second})
            .filterValues { discounts -> hasDiscountCount(discounts) > doesNotHaveDiscountCount(discounts) }
            .keys
}
// every trip can have multiple passengers
private fun passengersHaveDiscountInTrip() =
        { trip: Trip -> trip.passengers.map { passenger -> passenger to (trip.discount != null) } }

private fun doesNotHaveDiscountCount(discountList: List<Boolean>) =
        discountList.count { hasDiscount -> !hasDiscount }

private fun hasDiscountCount(discountList: List<Boolean>) =
        discountList.count { hasDiscount -> hasDiscount }


/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    if(this.trips.isEmpty()) return null

    val tripsFrequency = this.trips.map {
        val reminder = it.duration % 10;
        val lower = it.duration - reminder

        IntRange(lower, lower + 9)
    }

    return tripsFrequency
            .groupBy { it }
            .map { (key, value) -> key to value.size }
            .maxBy { it.second }?.first
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    val byDriver = this
            .trips
            .groupBy(keySelector = { it -> it.driver}, valueTransform = { it -> it.cost})
            .map { (key, value) -> key to value.sum() }
            .sortedByDescending { it-> it.second }

    val totalProfit = byDriver.map { it -> it.second }.sum()
    val totalDrivers = this.allDrivers.size
    val tpDriversCount = totalDrivers / 5

    val tpDriverProfit = byDriver.take(tpDriversCount).map { it -> it.second }.sum();

    return (tpDriverProfit / totalProfit) >= 0.8
}