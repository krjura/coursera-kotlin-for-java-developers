package nicestring

fun String.isNice(): Boolean {
    val substrings = setOf<String>("bu", "ba", "be");
    val vowels = setOf<Char>('a', 'e', 'i', 'o', 'u');

    val condition1 = substrings.none { this.contains(it, false) }
    val condition2 = count { vowels.contains(it) } >= 3
    val condition3 = zipWithNext().count { (f, s) -> f == s} > 0

    return listOf<Boolean>(condition1, condition2, condition3).count { it } >= 2
}