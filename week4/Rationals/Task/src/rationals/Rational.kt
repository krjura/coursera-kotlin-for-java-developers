package rationals

import java.math.BigInteger

data class Rational (val numerator: BigInteger, val denominator: BigInteger) : Comparable<Rational> {

    companion object {
        fun create(_numerator: BigInteger, _denominator: BigInteger): Rational {
            if(_denominator == BigInteger.ZERO) {
                throw IllegalArgumentException("denominator cannot be 0")
            }

            val gcd = _numerator.gcd(_denominator)

            val normalizedNumerator = _numerator.divide(gcd)
            val normalizedDenominator = _denominator.divide(gcd)

            return if(normalizedNumerator < BigInteger.ZERO && normalizedDenominator < BigInteger.ZERO) {
                Rational(normalizedNumerator.negate(), normalizedDenominator.negate())
            } else if(normalizedNumerator > BigInteger.ZERO && normalizedDenominator < BigInteger.ZERO) {
                Rational(normalizedNumerator.negate(), normalizedDenominator.negate())
            } else {
                Rational(normalizedNumerator, normalizedDenominator)
            }
        }
    }

    operator fun minus(other: Rational): Rational {
        return create(
                (this.numerator * other.denominator) + (-other.numerator * this.denominator),
                this.denominator * other.denominator
        )
    }

    operator fun plus(other: Rational): Rational {
        return create(
                (this.numerator * other.denominator) + (other.numerator * this.denominator),
                this.denominator * other.denominator
        )
    }

    operator fun div(other: Rational): Rational {
        return create(
                this.numerator * other.denominator,
                this.denominator * other.numerator
        )
    }

    operator fun times(other: Rational): Rational {
        return create(
                this.numerator * other.numerator,
                this.denominator * other.denominator
        )
    }

    operator fun unaryMinus(): Rational {
        return create(-this.numerator, this.denominator)
    }

    operator fun rangeTo(other: Rational): ClosedRange<Rational> {
        return RationalClosedRange(this, other)
    }

    override fun compareTo(other: Rational): Int {
        val firstNominator = this.numerator * other.denominator
        val secondNominator = other.numerator * this.denominator

        return firstNominator.compareTo(secondNominator)
    }

    override fun toString(): String {
        if(this.denominator == BigInteger.ONE) {
            return this.numerator.toString()
        }

        return "${this.numerator}/${this.denominator}"
    }
}

class RationalClosedRange(override val start: Rational, override val endInclusive: Rational) : ClosedRange<Rational>

infix fun Int.divBy(denominator: Int): Rational =
        Rational.create(BigInteger.valueOf(this.toLong()), BigInteger.valueOf(denominator.toLong()))

infix fun Long.divBy(denominator: Long): Rational =
        Rational.create(BigInteger.valueOf(this), BigInteger.valueOf(denominator))

infix fun BigInteger.divBy(denominator: BigInteger): Rational =
        Rational.create(this, denominator)

fun String.toRational(): Rational {
    val components = this.split('/')

    return when (components.size) {
        1 -> Rational.create(components[0].toBigInteger(), BigInteger.ONE)
        2 -> Rational.create(components[0].toBigInteger(), components[1].toBigInteger())
        else -> throw IllegalArgumentException("rational must be in the form <numerator>/<denominator>")
    }
}

fun main() {
    val half = 1 divBy 2
    val third = 1 divBy 3

    val sum: Rational = half + third
    println(5 divBy 6 == sum)

    val difference: Rational = half - third
    println(1 divBy 6 == difference)

    val product: Rational = half * third
    println(1 divBy 6 == product)

    val quotient: Rational = half / third
    println(3 divBy 2 == quotient)

    val negation: Rational = -half
    println(-1 divBy 2 == negation)

    println((2 divBy 1).toString() == "2")
    println((-2 divBy 4).toString() == "-1/2")
    println("117/1098".toRational().toString() == "13/122")

    val twoThirds = 2 divBy 3
    println(half < twoThirds)

    println(half in third..twoThirds)

    println(2000000000L divBy 4000000000L == 1 divBy 2)

    println("912016490186296920119201192141970416029".toBigInteger() divBy
            "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)
}