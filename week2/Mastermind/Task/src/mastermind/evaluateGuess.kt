package mastermind

import java.util.Arrays

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret: String, guess: String): Evaluation {
    val processedSecrets = Array<Boolean>(secret.length) { _ -> false}
    val processedGuesses = Array<Boolean>(secret.length) { _ -> false}

    var rightPosition = 0;
    var wrongPosition = 0;

    for (i in guess.indices) {
        if(guess[i] == secret[i]) {
            rightPosition++;
            processedSecrets[i] = true
            processedGuesses[i] = true
        }
    }

    for (i in guess.indices) {
        for (j in secret.indices) {
            if(processedGuesses[i]) {
                break
            }

            if(processedSecrets[j]) {
                continue
            }

            if(guess[i] == secret[j]) {
                wrongPosition++;

                processedGuesses[i] = true
                processedSecrets[j] = true
            }
        }
    }

    return Evaluation(rightPosition, wrongPosition);
}
